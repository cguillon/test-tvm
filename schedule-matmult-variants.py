#!/usr/bin/env python3
"""
Simple scheduling example for a matmult using TVM Tensor Expressions.

There are different schedule variants available with --strategy option.

See also TVM tutorial for more details:
https://tvm.apache.org/docs/tutorial/tensor_expr_get_started.html

Dependencies:
    - first build TVM in ~/tvm for instance
    - then install requirements
      pip install -r ~/tvm/python/requirements/core.txt
      pip install -r ~/tvm/python/requirements/xgboost.txt
      pip install -r requirements
    - then export python path
      export PYTHONPATH=$HOME/tvm/python

"""

import sys
import argparse
import numpy as np
import shutil
from pathlib import Path
import tarfile
import glob

import tvm
from tvm import te

# Define Matmul as a function returning a tvm te expression with input args
def matmul(N, M, K, dtype):
    # Define input tensors
    A = te.placeholder((N,K), name="A", dtype=dtype)
    B = te.placeholder((K,M), name="B", dtype=dtype)

    # Define output tensor, as an expression of inputs
    # Give shape, operation (as a lambda on an element) and name
    # Note that the reduction axis is specified for the te.sum() accumulator
    k = te.reduce_axis((0, K))
    C = te.compute((N,M), lambda i, j: te.sum(A[i, k] * B[k, j], axis=k))
    return C, A, B

# Return a default schedule for a tvm te output expression
def schedule_default(ops, params):
    # Create basic schedule for the output tensor operation
    sch = te.create_schedule(ops[0].op)
    return sch

def save_to_temp(name, content, args):
    if not args.save_temps:
        return
    tdir = Path(args.save_temps_dir)
    tdir.mkdir(exist_ok=True, parents=True)
    with open(tdir / name, "w") as outf:
        outf.write(content)

def save_schedule(sch, ops, name, args):
    if not args.save_temps:
        return
    sch_lowered = tvm.lower(sch, ops, simple_mode=True)
    save_to_temp(f"{name}.sch.txt", str(sch_lowered), args)

# Goto-Blas matmul schedling
def schedule_matmul_goto(ops, params):
    C, A, B = ops
    # Strategy IKJKIJ
    factor_i = params[0]
    factor_j = params[1]
    factor_k = params[2]
    sch = te.create_schedule(C.op)
    axis_i, axis_j = C.op.axis      # get axis list
    axis_k, = C.op.reduce_axis      # get reduce axis list
    axis_i_o, axis_i_in = sch[C].split(axis_i, factor=factor_i) # split into blocks of size 6
    axis_j_o, axis_j_in = sch[C].split(axis_j, factor=factor_j) # split into blocks of size 2x8
    axis_k_o, axis_k_in = sch[C].split(axis_k, factor=factor_k) # split into blocks of size 64
    sch[C].reorder(axis_i_o, axis_k_o, axis_j_o, axis_k_in, axis_i_in, axis_j_in) # reorder all axes
    sch[C].unroll(axis_i_in)       # unroll second loop
    sch[C].unroll(axis_j_in)       # unroll inner loop
    sch[C].vectorize(axis_j_in)    # vectorize inner loop
    return sch

def split(sch, axis, factors):
    splits = [axis]
    for factor in factors[::-1]:
        ax, splits[0] = sch.split(splits[0], factor=factor)
        splits.insert(0, ax)
    return splits

# Tile7d schedule (Ansor like) without buffer
def schedule_matmul_tile7d(ops, params):
    C, A, B = ops
    # Strategy is PPRPRP
    factors_i = params[0:3]
    factors_j = params[3:6]
    factors_k = params[6:7]
    sch = te.create_schedule(C.op)
    i, j = C.op.axis
    k, = C.op.reduce_axis
    i0, i1, i2, i3 = split(sch[C], i, factors=factors_i)
    j0, j1, j2, j3 = split(sch[C], j, factors=factors_j)
    k0, k1 = split(sch[C], k, factors=factors_k)
    sch[C].reorder(i0, j0, i1, j1, k0, i2, j2, k1, i3, j3)
    sch[C].unroll(k1)
    sch[C].unroll(i3)
    sch[C].unroll(j3)
    sch[C].vectorize(j3)
    return sch

# Tile7d schedule (Ansor like) with write_cache
def schedule_matmul_tile7d_wc(ops, params):
    C, A, B = ops
    # Strategy is PP|RPRP where | is the write cache
    factors_i = params[0:3]
    factors_j = params[3:6]
    factors_k = params[6:7]
    sch = te.create_schedule(C.op)
    WC = sch.cache_write(C, "local")
    i, j = C.op.axis
    i0, i1, ix = split(sch[C], i, factors=[factors_i[0], factors_i[1]*factors_i[2]])
    j0, j1, jx = split(sch[C], j, factors=[factors_j[0], factors_j[1]*factors_j[2]])
    sch[C].reorder(i0, j0, i1, j1, ix, jx)
    sch[WC].compute_at(sch[C], j1)
    ic, jc = sch[WC].op.axis
    kc, = sch[WC].op.reduce_axis
    i2, i3 = split(sch[WC], ic, factors=[factors_i[2]])
#    vecsizes = [16, 8, 4, 2]
#    vec_factors = (factors_j[2], 1)
#    for vecsize in vecsizes:
#        if factors_j[2] % vecsize == 0:
#            vec_factors = (factors_j[2]//vecsize, vecsize)
#            break
#    j2, j3, jv = split(sch[WC], jc, factors=[*vec_factors])
    j2, j3 = split(sch[WC], jc, factors=[factors_j[2]])
    k0, k1 = split(sch[WC], kc, factors=[factors_k[0]])
#    sch[WC].reorder(k0, i2, j2, k1, i3, j3, jv)
    sch[WC].reorder(k0, i2, j2, k1, i3, j3)
#    sch[WC].vectorize(jv)
#    sch[WC].unroll(j3)
    sch[C].vectorize(jx) # For the copy back
    sch[WC].vectorize(j3)
    sch[WC].unroll(i3)
    sch[WC].unroll(k1)
    return sch

def schedule_matmul_tile7d_wc2(ops, params):
    O = ops[0]
    #O = obj[-1]
    sch = te.create_schedule(O.op)
    O_W0 = sch.cache_write(O, "local")
    i, j, = O.op.axis
    k, = O.op.reduce_axis
    i, i1 = sch[O].split(i, factor=256)
    j, j1 = sch[O].split(j, factor=2048)
    i1, i_ = sch[O].split(i1, factor=128)
    j1, j_ = sch[O].split(j1, factor=64)
    sch[O].reorder(i, j, i1, j1, i_, j_)
    sch[O_W0].compute_at(sch[O], j1)
    i, j, = O_W0.op.axis
    k, = O_W0.op.reduce_axis
    i2 = i
    j2 = j
    k, k1 = sch[O_W0].split(k, factor=13)
    i2, i3 = sch[O_W0].split(i2, factor=4)
    j2, j3 = sch[O_W0].split(j2, factor=64)
    sch[O_W0].reorder(k, i2, j2, k1, i3, j3)
    sch[O_W0].vectorize(j3)
    sch[O_W0].unroll(i3)
    sch[O_W0].unroll(k1)
    return sch


def schedule_matmul_tile8d(ops, params):
    if params[7] == 1:
        return schedule_matmul_tile7d_wc(ops, params[0:7])
    else:
        return schedule_matmul_tile7d(ops, params[0:7])

# Build from a schedule, the out/in ops and a target
def build(sch, ops, tgt, name):
    # Build for the target
    # Give schedule, input/output tensors, target and name
    built = tvm.build(sch, ops, tgt, name=name)
    return built

def save_build(built, ops, name, args):
    if not args.save_temps:
        return
    built_ll = built.get_source("ll")
    save_to_temp(f"{name}.ll", str(built_ll), args)

def get_patch_dir():
    return Path(__file__).parent / "patches"

def get_templates_dir():
    return Path(__file__).parent / "templates"

def get_static_dir():
    return Path(__file__).parent / "static"

def generate_file(file_path, template_path, **kwargs) -> None:
    from jinja2 import Environment, FileSystemLoader
    file_path = Path(file_path)
    template_path = Path(template_path)
    file_path.parent.mkdir(parents=True, exist_ok=True)
    template = Environment(loader=FileSystemLoader(
        template_path.parent)).get_template(template_path.name)
    with open(file_path, mode="w", encoding="utf-8") as file:
        file.write(template.render(kwargs))

def apply_patch(patch_file, cwd="."):
    import subprocess
    with open(patch_file) as pf:
        subprocess.run(["patch", "-p1"], text=True, check=True, stdin=pf, stdout=subprocess.DEVNULL, cwd=cwd)

# Build Export from a schedule
def export_c(sch, ops, op_args, arch, name, **kwargs):
    from tvm.relay.backend import Executor, Runtime
    from tvm.micro import export_model_library_format, get_standalone_crt_dir, get_microtvm_template_projects
    export_dir = Path(f"export.{name}")
    print(f"Generating Export in {export_dir} ...")
    runtime = Runtime("crt", {"system-lib": True})
    arch_target_c = {
        "default": "c -keys=cpu",
        "avx2": "c -keys=cpu -mcpu=core-avx2",
        "avx512": "c -keys=cpu -mcpu=skylake-avx512",
        "aarch64": "c -keys=arm_cpu,cpu -mcpu=cortex-a72 -march=armv8-a",
    }
    tgt = tvm.target.Target(arch_target_c[arch])
    exe = tvm.relay.backend.Executor(
        "aot", {"unpacked-api": True, "interface-api": "c", "workspace-byte-alignment": 8}
    )
    config = {
        "tir.disable_vectorize": True,
    }
    with tvm.transform.PassContext(opt_level=3, config=config):
        # Build for the target
        # Give schedule, input/output tensors, target and name
        built = tvm.build(sch, ops, target=tgt, name=name)
    shutil.rmtree(export_dir, ignore_errors=True)
    export_dir.mkdir(exist_ok=True)
    export_model_library_format(built, str(export_dir / "model.tar"))
    templates_dir = get_microtvm_template_projects("crt")
    shutil.copytree(templates_dir, export_dir / "app")
    crt_dir = get_standalone_crt_dir()
    shutil.copytree(crt_dir, export_dir / "app" / "crt")
    with tarfile.open(export_dir / "model.tar") as tf:
        tf.extractall(export_dir / "app" / "model")
    i, j, k, dtype = op_args
    dtype_bits = {
        "float32": 32,
        "float64": 64,
    }
    build_config = {
        "tvm_workspace_size_bytes": 128*1024*1024,
        "tvm_crt_debug": 0, # 2 for debug output
        "arch_ipc": kwargs["arch_ipc"],
        "arch_vecbits": kwargs["arch_vsize"],
        "arch_freq": kwargs["arch_freq"]*1e9,
        "comp_num": i*j*k,
        "comp_eltbits": dtype_bits[dtype],
        "comp_name": name,
        "inouts": [
            {
                "name": "C",
                "shape": (i, j),
                "dtype": dtype,
            },
            {
                "name": "A",
                "shape": (i, k),
                "dtype": dtype,
            },
            {
                "name": "B",
                "shape": (k, j),
                "dtype": dtype,
            },
        ],
    }
    templ_dir = get_templates_dir() / "app"
    generate_file(export_dir / "app" / "crt_config" / "crt_config.h", templ_dir / "crt_config.h.jinja", **build_config)
    generate_file(export_dir / "app" / "CMakeLists.txt", templ_dir / "CMakeLists.txt.jinja", **build_config)
    generate_file(export_dir / "app" / "src" / "helpers.cc", templ_dir / "helpers.cc.jinja", **build_config)
    generate_file(export_dir / "app" / "include" / "platform_arch.h", templ_dir / "platform_arch.h.jinja", **build_config)
    generate_file(export_dir / "app" / "include" / "compute.h", templ_dir / "compute.h.jinja", **build_config)
    generate_file(export_dir / "app" / "src" / "model_forward.cc", templ_dir / "model_forward.cc.jinja", **build_config)
    static_dir = get_static_dir()
    shutil.copy(static_dir / "app" / "main.cc", export_dir / "app" / "src")
    shutil.copy(static_dir / "app" / "ndutils.h", export_dir / "app" / "include")
    shutil.copy(static_dir / "app" / "ndutils.c", export_dir / "app" / "src")
    shutil.copy(static_dir / "app" / "tvmutils.c", export_dir / "app" / "src")
    shutil.copy(static_dir / "app" / "timerutils.h", export_dir / "app" / "include")
    shutil.copy(static_dir / "app" / "timerutils.c", export_dir / "app" / "src")
    shutil.copy(static_dir / "app" / "model_forward.h", export_dir / "app" / "include")
    patch_dir = get_patch_dir()
    for patch in glob.glob(f"{patch_dir}/*.patch"):
        apply_patch(patch, cwd=export_dir / "app")
    return built

def run_cmd(cmd, cwd="."):
    import subprocess
    import shlex
    print(f"Executing: (cd {cwd} && {cmd}) ...")
    cmdline = shlex.split(cmd)
    subprocess.run(cmdline, text=True, check=True, stdin=subprocess.DEVNULL, cwd=cwd)

# Build export
def build_export_c(arch, name):
    import subprocess
    cwd = Path(f"export.{name}") / "app"
    print(f"Building Exported app in {cwd} ...")
    run_cmd("cmake -G Ninja .", cwd=cwd)
    run_cmd("ninja", cwd=cwd)

# Execute export
def run_export_c(arch, name):
    import subprocess
    cwd = Path(f"export.{name}") / "app"
    print(f"Running Exported app in {cwd} ...")
    run_cmd("./main", cwd=cwd)

# Helper to generate matmul inputs/output tests
def gen_matmul_tvm_data(args, dev):
    N, M, K, dtype = args
    # Test generaing inputs/outputs for the device and executing function
    c = tvm.nd.array(np.zeros(N*M, dtype=dtype).reshape((N, M)), dev)
    a = tvm.nd.array(np.random.uniform(size=N*K).astype(dtype).reshape((N, K)), dev)
    b = tvm.nd.array(np.random.uniform(size=K*M).astype(dtype).reshape((K, M)), dev)
    return c, a, b

# Evaluate function on device an return msecs
def evaluate(func, func_args, device):
    repeat, number, min_repeat_ms = 5, 1, 100
    evaluator = func.time_evaluator(
        func.entry_name,
        device,
        repeat=repeat,
        number=number,
        min_repeat_ms=min_repeat_ms,
    )
    results = evaluator(*func_args).results
    return min(results) * 1000

def get_peak_ms(op_args, freq, ipc, vsize):
    N, M, K, dtype = op_args
    vecelts = vsize//np.dtype(dtype).itemsize//8
    comp = N * M * K
    peak_ms = comp / (freq*1e6) / ipc / vecelts
    return peak_ms

def main():
    archs = {
        "default": "llvm",
        "avx2": "llvm -mcpu=core-avx2",
        "avx512": "llvm -mcpu=skylake-avx512",
        "aarch64": "llvm -mtriple=aarch64-linux-gnu -mcpu=cortex-a72 -mattr=+neon",
    }
    vsizes = {
        "default": 128,
        "avx2": 256,
        "avx512": 512,
        "aarch64": 128,
    }
    schedules = {
        "default": schedule_default,
        "goto": schedule_matmul_goto,
        "tile7d": schedule_matmul_tile7d,
        "tile8d": schedule_matmul_tile8d,
    }
    default_params = {
        "default": {
            "default": [],
        },
        "goto": {
            "default": [4, 64, 13],
            "aarch64": [4, 16, 13],
        },
        "tile7d": {
            "default": [2, 32, 4, 32, 1, 64, 13],
            "aarch64": [1, 32, 4, 1, 1, 16, 13],
        },
        "tile8d": {
            "default": [2, 32, 4, 32, 1, 64, 13, 1],
            "aarch64": [1, 32, 4, 1, 1, 16, 13, 1], # 53.29% peak perf on RPI4
            #"aarch64": [2, 64, 2, 64, 2, 8, 13, 1], # found by Ansor on 577/640 iterations 49.52% peak perf on RPI4

        },
    }
    default_sizes = [256, 2048, 1001]

    parser = argparse.ArgumentParser(
        description="Simple matmult with some scheduling strategies",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("--arch", type=str, choices=list(archs), default="default", help="Architecture")
    parser.add_argument("--freq", type=float, default="2.1", help="Freq Ghz")
    parser.add_argument("--ipc", type=int, default="2", help="FMA/cycle")
    parser.add_argument("--schedule", type=str, choices=list(schedules), default="default", help="Schedules")
    parser.add_argument("--dims", type=int, nargs=3, default=default_sizes, help="Matmul N M K sizes")
    parser.add_argument("--params", type=int, nargs='*', help="Schedule paramaters")
    parser.add_argument("--dtype", type=str, choices=["float32", "float64"], default="float32", help="Data type")
    parser.add_argument("--export", action="store_true", help="Export C build system")
    parser.add_argument("--run-export", action="store_true", help="Export C build system, build and run")
    parser.add_argument("--save-temps", action="store_true", help="Save temps")
    parser.add_argument("--save-temps-dir", type=str, default="tmp.save", help="Save temps dir")
    args = parser.parse_args()

    # Matmul shape for C[N,M] = matmult(A[N,K], B[K,M])
    op_args = tuple([*args.dims, args.dtype])

    # Define target (needed for build)
    target = archs[args.arch]
    tgt = tvm.target.Target(target=target)

    # Define a device to run on (needed for run), here the host CPU
    dev = tvm.device(tgt.kind.name, 0)

    # Define operation and schedule
    schedule = schedules[args.schedule]
    params_dict = default_params[args.schedule]
    params = args.params if args.params is not None else params_dict.get(args.arch, params_dict["default"])
    ops = matmul(*op_args)
    sch = schedule(ops, params)

    # op name
    name = "test_matmul"

    # Save schedule on --save-temps
    save_schedule(sch, ops, name, args)

    # Build
    test_matmul = build(sch, ops, tgt, name=name)

    # Save build on --save-temps
    save_build(test_matmul, ops, name, args)

    # Optional export
    if args.export or args.run_export:
        export_c(sch, ops, op_args, args.arch, name=name,
                 arch_ipc=args.ipc, arch_freq=args.freq, arch_vsize=vsizes[args.arch])
        if args.run_export:
            build_export_c(args.arch, name=name)
            run_export_c(args.arch, name=name)
        return

    # Here we run (note that compilation may happen also as it is lazy)
    c, a, b = gen_matmul_tvm_data(op_args, dev)
    test_matmul(c, a, b)

    # Assert result is what we expect
    ref = a.numpy() @ b.numpy()
    assert np.allclose(c.numpy(), ref), f"the computed result mismatchs"
    print("Verified correct")

    # Evaluate performance
    time_ms = evaluate(test_matmul, (c, a, b), dev)
    peak_ms = get_peak_ms(op_args, args.freq, args.ipc, vsizes[args.arch])
    print(f"Args: {args.dims} {args.dtype}")
    print(f"Arch: {args.arch}, {args.freq} Ghz, {args.ipc} FMA/c")
    print(f"Elapsed ms: {time_ms:.3f}")
    print(f"Peak ms: {peak_ms:.2f}")
    print(f"Peak %: {peak_ms/time_ms*100:.2f}")

if __name__ == "__main__":
    main()
