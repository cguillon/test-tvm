TVM Schedule examples
=====================

Simple examples of TVM schedules:

- schedule-vecadd.py: a vector add with vectorization and parallelization.
- schedule-matmul.py: a matrix multiply with tiling, vectorization, parallelization and unroll.
- schedule-matmult-goto.py: Goto strategy with a single level of tiling
- schedule-convolution.py: a 2D convolution  with tiling, vectorization, parallelization and unroll.

Example with several strategies:

- schedule-matmult-variants.py: goto, tile7d, tile8d strategies with predefined paramaters

Examples with TVM Ansor auto tuner:

- schedule-matmult-auto.py: autotuned matmult
- schedule-convolution-auto.py: autotuned matmult


Install
=======

First install TVM, for instance in `~/tvm`.

Then install python dependencies and add tvm in Python path:

    python3 -m venv .venv
    source .venv/bin/activate
    pip install -r ~/tvm/python/requirements/core.txt
    pip install -r ~/tvm/python/requirements/xgboost.txt
    pip install -r requirements
    export PYTHONPATH=$HOME/tvm/python

Test for instance a simple matmul schedule with:

    ./schedule-matmult.py
    ...
    All went well


Experiment with fixed schedule strategy
=======================================

Experiment different MatMult scheduling strategy, for instance:
- goto: simple strategy with one level of tiling for each dimension
- tile7d: multiple level of tilings
- tile8d: tile7d + write buffer

For instance (run on pinocchio):

    ./schedule-matmult-variants.py --arch avx512 --freq 2.1 --ipc 2 --schedule goto
    ...
    Peak %: 30.58

    ./schedule-matmult-variants.py --arch avx512 --freq 2.1 --ipc 2 --schedule tile7d
    ...
    Peak %: 49.65

    ./schedule-matmult-variants.py --arch avx512 --freq 2.1 --ipc 2 --schedule tile8d
    ...
    Peak %: 70.08


Experiments with TVM Ansor
==========================

Ref to `experiments` dir for experiments.


TVM Export Mode
===============

One can export a generated build system with C/C++ code based on micro TVM export.

For instance:

    ./schedule-matmult-variants.py --arch avx512 --freq 2.1 --ipc 2 --schedule tile8d --export

This will generate an `export.test_matmul/app` directory which can be built and run with:

    cd export.test_matmul/app
    cmake .
    make -j 8
    ./main
    ....

Also the `--run-export` option can export/build/run as in:

    ./schedule-matmult-variants.py --arch avx512 --freq 2.1 --ipc 2 --schedule tile8d --run-export
    ...
    peak_ms: 7.80971
    elpased_ms: 63.99
    peak_pct: 12.2046

The source file of the application is located in `static/app/main.cc`.

Note that due to limitations in the micro TVM C code generator, vectorization is disabled,
hence the low peak performance reported.
