#!/usr/bin/env python3
"""
Simple scheduling example for a matmult using TVM Tensor Expressions.

See also TVM tutorial for more details:
https://tvm.apache.org/docs/tutorial/tensor_expr_get_started.html

Dependencies:
    - first build TVM in ~/tvm for instance
    - then install requirements
      pip install ~/tvm/python/requirements/core.txt
      pip install numpy
    - then export python path
      export PYTHONPATH=$HOME/tvm/python

"""

import tvm
from tvm import te
import numpy as np

# Define target (needed for build), here the host CPU archi built with llvm
tgt = tvm.target.Target(target="llvm", host="llvm")

# Define a device to run on (needed for run), here the host CPU
dev = tvm.device(tgt.kind.name, 0)

# Define input tensors
# Give shape and name
N, K, M = 8, 256, 512
A = te.placeholder((N,K), name="A")
B = te.placeholder((K,M), name="B")

# Define output tensor, as an expression of inputs
# Give shape, operation (as a lambda on an element) and name
# Note that the reduction axis is specified for the te.sum() accumulator
k = te.reduce_axis((0, K))
C = te.compute((N,M), lambda i, j: te.sum(A[i, k] * B[k, j], axis=k))

# Create basic schedule for the output tensor operation
s = te.create_schedule(C.op)

# START OF Scheduling (this is optional)
axis_i, axis_j = C.op.axis      # get axis list
axis_k, = C.op.reduce_axis      # get reduce axis list
axis_i_o, axis_i_in = s[C].split(axis_i, factor=6) # split into blocks of size 6
axis_j_o, axis_j_in = s[C].split(axis_j, factor=16) # split into blocks of size 2x8
axis_k_o, axis_k_in = s[C].split(axis_k, factor=64) # split into blocks of size 64
s[C].reorder(axis_i_o, axis_k_o, axis_j_o, axis_k_in, axis_i_in, axis_j_in) # reorder all axes
s[C].vectorize(axis_j_in)    # vectorize inner loop
s[C].unroll(axis_j_in)       # unroll inner loop
s[C].unroll(axis_i_in)       # unroll second loop
# END OF Scheduling

# Build for the target
# Give schedule, input/output tensors, target and name
test_matmult = tvm.build(s, [A, B, C], tgt, name="test_matmult")

# That's it now the test_add() function is a wrapper to the
# generated implementation

# Test generaing inputs/outputs for the device and executing function
a = tvm.nd.array(np.random.uniform(size=N*K).astype(A.dtype).reshape((N, K)), dev)
b = tvm.nd.array(np.random.uniform(size=K*M).astype(B.dtype).reshape((K, M)), dev)
c = tvm.nd.array(np.zeros(N*M, dtype=C.dtype).reshape((N, M)), dev)

# Here we run (note that compilation may happen also as it is lazy)
test_matmult(a, b, c)

# Assert result is what we expect
assert np.allclose(c.numpy(), a.numpy() @ b.numpy()), f"the computed result mismatchs"
print("All went well")

