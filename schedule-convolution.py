#!/usr/bin/env python3
"""
Simple scheduling example for a convolution+bias+relu TVM Tensor Expressions.

See also TVM tutorial for more details:
https://tvm.apache.org/docs/tutorial/tensor_expr_get_started.html

Dependencies:
    - first build TVM in ~/tvm for instance, ref to https://gitlab.inria.fr/CORSE/parioopt#step-2-install-tvm
    - then install requirements
      pip install -r ~/tvm/python/requirements/core.txt
      pip install -r requirements.txt
    - then export python path
      export PYTHONPATH=$HOME/tvm/python

"""

import tvm
from tvm import te
from tvm import auto_scheduler
import numpy as np
import argparse

parser = argparse.ArgumentParser(description="convolution schedule")
parser.add_argument("--schedule", action='store_true', help="apply a predefined schedule")
args = parser.parse_args()

# Define target (needed for build), here the host CPU archi built with llvm
tgt = tvm.target.Target(target="llvm", host="llvm")

# Define a device to run on (needed for run), here the host CPU
dev = tvm.device(tgt.kind.name, 0)

# Convolution parameters (X and Y are size of output)
X, Y, CIN, COUT = 112, 112, 32, 32
SX = SY = 1
KX = KY = 3
AX, AY = (X-1)*SX + KX-1 + 1, (Y-1)*SY + KY-1 + 1


def convolution(X, Y, CIN, COUT, KX, KY, SX, SY, dtype):
    # Define input tensors
    A = te.placeholder((AX, AY, CIN), name="A", dtype=dtype)
    W = te.placeholder((KX, KY, CIN, COUT), name="W", dtype=dtype)

    # Define output tensor, as an expression of inputs
    # Give shape, operation (as a lambda on an element) and name
    # Note that the reduction axis is specified for the te.sum() accumulator
    cin = te.reduce_axis((0, CIN), "cin")
    kx = te.reduce_axis((0, KX), "kx")
    ky = te.reduce_axis((0, KY), "ky")
    CONV = te.compute((X, Y, COUT), lambda x, y, cout: te.sum(A[x * SX + kx, y * SY + ky, cin] * W[kx, ky, cin, cout], axis=[kx, ky, cin]), name="CONV")
    return [A, W, CONV]

conv_args = (X, Y, CIN, COUT, KX, KY, SX, SY, "float32")
out = convolution(*conv_args)
A, W, C = out

def manual_schedule_conv(s, C):
    # Manual schedule, optional
    axis_x, axis_y, axis_cout = C.op.axis      # get axis list
    axis_kx, axis_ky, axis_cin = C.op.reduce_axis      # get reduce axis list
    axis_y_outer, axis_y_inner = s[C].split(axis_y, nparts=8) # split into 8 blocks
    axis_cout_outer, axis_cout_inner = s[C].split(axis_cout, factor=16) # split into inner blocks of 16
    axis_cin_outer, axis_cin_inner = s[C].split(axis_cin, factor=8) # split into inner block of 8
    s[C].reorder(axis_x, axis_y_outer, axis_cin_outer, axis_cout_outer, axis_y_inner, axis_cin_inner, axis_kx, axis_ky, axis_cout_inner) # reorder all axes
    fused = s[C].fuse(axis_x, axis_y_outer)  # fuse outer axes
    s[C].parallel(fused)            # parallelize outer loop (of fused len 8*8)
    s[C].vectorize(axis_cout_inner) # vectorize inner loop
    s[C].unroll(axis_ky)    # unroll inner loops
    s[C].unroll(axis_kx)


# Create basic schedule for the output tensor operation
s = te.create_schedule(C.op)

if args.schedule:
    manual_schedule_conv(s, C)

# Output schedule
print(tvm.lower(s, out, simple_mode=True))


# Build for the target
# Give schedule, input/output tensors, target and name
test_conv = tvm.build(s, out, tgt, name="test_conv")

# That's it now the test_conv() function is a wrapper to the
# generated implementation

# Test generaing inputs/outputs for the device and executing function
a = tvm.nd.array(np.random.uniform(size=AX*AY*CIN).astype(A.dtype).reshape((AX, AY, CIN)), dev)
w = tvm.nd.array(np.random.uniform(size=KX*KY*CIN*COUT).astype(W.dtype).reshape((KX, KY, CIN, COUT)), dev)
c = tvm.nd.array(np.zeros(X*Y*COUT, dtype=C.dtype).reshape((X,Y,COUT)), dev)

# wrap with an evaluation function for timings
eval_func = test_conv.time_evaluator(test_conv.entry_name, dev,
                                    repeat=10,
                                    number=1,
                                    min_repeat_ms=100)

# Here we run (note that compilation may happen here as it is lazy)
results = eval_func(a, w, c)
print(f"Execution time (ms): {results.mean*1000}")

# Assert result is what we expect: TODO: implement in numpy
#assert np.allclose(c.numpy(), a.numpy() @ b.numpy()), f"the computed result mismatchs"
#print("All went well")

