#!/usr/bin/env bash
set -euo pipefail
pidfile="$HOME"/.tvm.ssh_relay.pid
if [ -e "$pidfile" ]; then
    pid="$(cat "$pidfile")"
    echo "Killing $pid from $pidfile..."
    exists=true
    kill "$pid" || exists=false
    while $exists; do
        sleep 1
        kill "$pid" 2>/dev/null || exists=false
    done
    rm "$pidfile"
fi

echo "Launching ssh relay..."
host="${1-corse-rpi0}"
setsid -- ptimeout 0 \
       ssh -oTCPKeepAlive=no -oServerAliveInterval=0 -n \
       -L9190:127.0.0.1:9190 \
       -L9090:127.0.0.1:9090 \
       -L9091:127.0.0.1:9091 \
       -L9092:127.0.0.1:9092 \
       -L9093:127.0.0.1:9093 \
       "$host" sleep infinity </dev/null >/dev/null 2>&1 &
pid=$!
echo "Saving $pid to $pidfile..."
echo "$pid" >"$pidfile"
