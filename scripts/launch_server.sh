#!/usr/bin/env bash
#
# Run on the target board (or host) the rpc tracker
#
set -euo pipefail
pidfile="$HOME"/.tvm.rpc_server.pid
logfile="$HOME"/.tvm.rpc_server.log
if [ -e "$pidfile" ]; then
    pid="$(cat "$pidfile")"
    echo "Killing $pid from $pidfile..."
    exists=true
    kill "$pid" || exists=false
    while $exists; do
        sleep 1
        kill "$pid" 2>/dev/null || exists=false
    done
    rm "$pidfile"
fi

echo "Launching rpc server..."
setsid -- ptimeout 0 \
       env TVM_NUM_THREADS=1 \
       python3 -m tvm.exec.rpc_server \
       --tracker=127.0.0.1:9190 \
       --key aarch64 \
       </dev/null >"$logfile" 2>&1 &
pid=$!
echo "Saving $pid to $pidfile..."
echo "$pid" >"$pidfile"
