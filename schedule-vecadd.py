#!/usr/bin/env python3
"""
Simple scheduling example for a vector add using TVM Tensor Expressions.

See also TVM tutorial for more details:
https://tvm.apache.org/docs/tutorial/tensor_expr_get_started.html

Dependencies:
    - first build TVM in ~/tvm for instance
    - then install requirements
      pip install ~/tvm/python/requirements/core.txt
      pip install numpy
    - then export python path
      export PYTHONPATH=$HOME/tvm/python

"""

import tvm
from tvm import te
import numpy as np

# Define target (needed for build), here the host CPU archi built with llvm
tgt = tvm.target.Target(target="llvm", host="llvm")

# Define a device to run on (needed for run), here the host CPU
dev = tvm.device(tgt.kind.name, 0)

# Define input tensors
# Give shape and name
N = 2**15
A = te.placeholder((N,), name="A")
B = te.placeholder((N,), name="B")

# Define output tensor, as an expression of inputs
# Give shape, operation (as a lambda on an element) and name
C = te.compute((N,), lambda i: A[i] + B[i], name="C")

# Create basic schedule for the output tensor operation
s = te.create_schedule(C.op)

# START OF Scheduling (this is optional)
axis_i, = C.op.axis      # get axis list
axis_i_outer, axis_i_inner = s[C].split(axis_i, nparts=8) # split into 8 blocks, into two axes
s[C].parallel(axis_i_outer)     # parallelize outer loop (of len 8)
s[C].vectorize(axis_i_inner)     # vectorize inner loop

# END OF Scheduling

# Build for the target
# Give schedule, input/output tensors, target and name
test_add = tvm.build(s, [A, B, C], tgt, name="test_add")

# That's it now the test_add() function is a wrapper to the
# generated implementation

# Test generaing inputs/outputs for the device and executing function
a = tvm.nd.array(np.random.uniform(size=N).astype(A.dtype), dev)
b = tvm.nd.array(np.random.uniform(size=N).astype(B.dtype), dev)
c = tvm.nd.array(np.zeros(N, dtype=C.dtype), dev)

# Here we run (note that compilation may happen also as it is lazy)
test_add(a, b, c)

# Assert result is what we expect
assert np.allclose(c.numpy(), a.numpy() + b.numpy()), f"the computed result mismatchs"
print("All went well")

