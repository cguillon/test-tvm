#!/usr/bin/env python3
"""
Simple scheduling example for a convolution+bias+relu TVM Tensor Expressions.

See also TVM tutorial for more details:
https://tvm.apache.org/docs/tutorial/tensor_expr_get_started.html

Dependencies:
    - first build TVM in ~/tvm for instance, ref to https://gitlab.inria.fr/CORSE/parioopt#step-2-install-tvm
    - then install requirements
      pip install -r ~/tvm/python/requirements/core.txt
      pip install -r requirements.txt
    - then export python path
      export PYTHONPATH=$HOME/tvm/python

Command line, for instance:
    # aarch64:
    ./schedule-matmult-auto.py  --freq 1.5 --ipc 1 --arch aarch64 --dims 256 2048 1001
    ./schedule-matmult-auto.py  --freq 1.5 --ipc 1 --arch aarch64 --dims 256 2048 1001 --auto-search --auto --auto-reset --epoch 10 --debug
    # AVX512:
    ./schedule-matmult-auto.py  --freq 2.1 --ipc 2 --arch avx512 --dims 256 2048 1001

"""

import sys
import os
import subprocess
import multiprocessing
import tvm
from tvm import te
from tvm import auto_scheduler
import numpy as np
from pathlib import Path
import logging
import argparse

logger = logging.getLogger(__name__)

@auto_scheduler.register_workload
def tmatmulf(N, M, K, dtype):
    A = te.placeholder((N, K), name="A", dtype=dtype)
    B = te.placeholder((M, K), name="B", dtype=dtype)

    k = te.reduce_axis((0, K), name="k")
    out = te.compute(
        (N, M),
        lambda i, j: te.sum(A[i, k] * B[j, k], axis=k),
        name="out",
        attrs={"layout_free_placeholders": [B]},  # enable automatic layout transform for tensor B
    )
    return [out, A, B]

@auto_scheduler.register_workload
def tmatmul(N, M, K, dtype):
    A = te.placeholder((N, K), name="A", dtype=dtype)
    B = te.placeholder((M, K), name="B", dtype=dtype)

    k = te.reduce_axis((0, K), name="k")
    out = te.compute(
        (N, M),
        lambda i, j: te.sum(A[i, k] * B[j, k], axis=k),
        name="out",
    )
    return [out, A, B]

@auto_scheduler.register_workload
def matmulf(N, M, K, dtype):
    A = te.placeholder((N, K), name="A", dtype=dtype)
    B = te.placeholder((K, M), name="B", dtype=dtype)

    k = te.reduce_axis((0, K), name="k")
    out = te.compute(
        (N, M),
        lambda i, j: te.sum(A[i, k] * B[k, j], axis=k),
        name="matmul",
        attrs={"layout_free_placeholders": [B]},  # enable automatic layout transform for tensor B
    )
    return [out, A, B]


@auto_scheduler.register_workload
def matmul(N, M, K, dtype):
    A = te.placeholder((N, K), name="A", dtype=dtype)
    B = te.placeholder((K, M), name="B", dtype=dtype)

    k = te.reduce_axis((0, K), name="k")
    out = te.compute(
        (N, M),
        lambda i, j: te.sum(A[i, k] * B[k, j], axis=k),
        name="matmul",
    )
    return [out, A, B]


def matmul_mintime(N, M, K, dtype, gflops):
    ops = N * K * M
    min_time = ops/gflops/1e9
    return min_time

def ref_matmul(N, M, K, dtype):
    a = np.random.uniform(size=(N, K)).astype(dtype)
    b = np.random.uniform(size=(K, M)).astype(dtype)
    return a @ b, (a, b)

def ref_tmatmul(N, M, K, dtype):
    a = np.random.uniform(size=(N, K)).astype(dtype)
    b = np.random.uniform(size=(M, K)).astype(dtype)
    return a @ b.T, (a, b)

def search_best(task, args):
    class LogCallback(auto_scheduler.measure.PythonBasedMeasureCallback):
        def callback(self, policy, inputs, results):
            for measure in results:
                time = np.mean([r.value for r in measure.costs])
                logger.debug(f"Execution time of candidate (ms): {time*1000:.3f}")

    if args.auto_reset:
        logger.debug(f"Clear schedules file {args.auto_file}")
        Path(args.auto_file).unlink(missing_ok=True)
    logger.debug(f"Search best schedule, store to {args.auto_file}")
    verbose = 2 if args.debug else 0
    batch, epoch, auto_file = args.batch, args.epoch, args.auto_file
    runner_opts = dict(repeat=1, number=1, min_repeat_ms=0, timeout=args.run_timeout)
    if args.remote:
        remote = args.remote.split(":")
        host, port = (remote[0], int(remote[1])) if len(remote) > 1 else (remote[0], 9190)
        runner = auto_scheduler.RPCRunner(key=args.arch, host=host, port=port, **runner_opts)
    else:
        runner = auto_scheduler.LocalRunner(**runner_opts)
    builder_opts = dict(timeout=args.build_timeout, n_parallel=args.build_jobs)
    builder = auto_scheduler.LocalBuilder(**builder_opts)
    tune_option = auto_scheduler.TuningOptions(
        num_measure_trials=batch * epoch,
        num_measures_per_round=batch,
        runner=runner,
        builder=builder,
        measure_callbacks=[auto_scheduler.RecordToFile(auto_file), LogCallback()],
        verbose=verbose,
    )
    cost_model = auto_scheduler.XGBModel(num_warmup_sample=batch)  # default: 100
    search_policy = auto_scheduler.SketchPolicy(
        task,
        cost_model,
        seed=0,
        verbose=verbose,
        params=dict(
            sample_init_min_population=50,  # default: 50
            evolutionary_search_population=2048,  # default: 2048
            evolutionary_search_num_iters=4,  # default: 4
            sample_init_use_measured_ratio=0.2,  # default: 0.2
        ),
    )
    task.tune(tune_option, search_policy=search_policy)

def launch_child(argv, args):
    env = {"TVM_NUM_THREADS": str(args.threads)}
    cmd = ["env", *(f"{k}={v}" for k, v in env.items()), "setarch", "-R", "--", argv[0], "--child", *argv[1:]]
    logger.debug("Executing child command: %s", ' '.join(cmd))
    proc = subprocess.run(args=cmd)
    if proc.returncode != 0:
        print(f"ERROR: running subprocess: exit code: {proc.returncode}: {' '.join(cmd)}", file=sys.stderr)
    raise SystemExit(proc.returncode)

def main():
    vsizes = {
        "default": 128,
        "avx2": 256,
        "avx512": 512,
        "aarch64": 128,
    }
    operators = {
        "matmul": (matmul, ref_matmul, matmul_mintime),
        "matmulf": (matmulf, ref_matmul, matmul_mintime),
        "tmatmul": (tmatmul, ref_tmatmul, matmul_mintime),
        "tmatmulf": (tmatmulf, ref_tmatmul, matmul_mintime),
    }
    archs = {
        "default": "llvm",
        "avx2": "llvm -mcpu=core-avx2",
        "avx512": "llvm -mcpu=skylake-avx512",
        "aarch64": "llvm -mtriple=aarch64-linux-gnu -mcpu=cortex-a72 -mattr=+neon",
    }
    default_sizes = [512, 256, 128]

    parser = argparse.ArgumentParser(
        description="convolution schedule",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    cpu_count = multiprocessing.cpu_count()
    parser.add_argument("--arch", type=str, choices=list(archs), default="default", help="Architecture")
    parser.add_argument("--dims", type=int, nargs=3, default=default_sizes, help="Matmul N M K sizes")
    parser.add_argument("--dtype", type=str, choices=["float32", "float64"], default="float32", help="Data type")
    parser.add_argument("--auto", action="store_true", help="use auto-scheduler schedule")
    parser.add_argument("--auto-search", action="store_true", help="search solutions with auto-scheduler")
    parser.add_argument("--auto-reset", action="store_true", help="reset auto-scheduler search")
    parser.add_argument("--auto-file", default="search-{operator}.log", help="auto scheduler search log file")
    parser.add_argument("--batch", type=int, default=64, help="batch size")
    parser.add_argument("--epoch", type=int, default=5, help="number of epochs")
    parser.add_argument("--function", type=str, choices=["matmul", "tmatmul", "tmatmulnf"], default="matmul", help="function to tune")
    parser.add_argument("--threads", type=int, default=1, help="threads")
    parser.add_argument("--ipc", type=int, default=2, help="machine FMAs per cycles")
    parser.add_argument("--freq", type=float, default=2.1, help="machine frequency Ghz")
    parser.add_argument("--dump-file", type=str, default="search-{operator}-dump.txt", help="output dump file")
    parser.add_argument("--dump-py-file", type=str, default="search-{operator}-py.txt", help="output dump py file")
    parser.add_argument("--module-file", type=str, default="search-{operator}.so", help="output module file")
    parser.add_argument("--remote", type=str, help="remote RPC tracker as host:port for rpc execution")
    parser.add_argument("--run-timeout", type=int, help="run timeout, inferred as: estimated peak_time * 20")
    parser.add_argument("--build-timeout", type=int, default=10, help="build timeout")
    parser.add_argument("--build-jobs", type=int, default=cpu_count, help="build jobs")
    parser.add_argument("--child", action="store_true", help="internal flag for marking child execution")
    parser.add_argument("--debug", action="store_true", help="debug mode")
    args = parser.parse_args()

    logging.basicConfig()
    if args.debug:
        logger.setLevel(logging.DEBUG)

    if not args.child:
        launch_child(sys.argv, args)

    # Check threads
    assert int(os.environ.get("TVM_NUM_THREADS", 0)) == args.threads, f"TVM_NUM_THREADS must be set to same value as --threads option"

    # Define target (needed for build), here the host CPU archi built with llvm
    target_str = archs[args.arch]
    target = tvm.target.Target(target=target_str)

    # Define a device to run on (needed for run), here the host CPU
    dev = tvm.device(target.kind.name, 0)

    # Convolution parameters (X and Y are size of output)
    N, M, K = args.dims
    tuned_args = (N, M, K, args.dtype)

    tuned_func, ref_func, tuned_mintime = operators[args.function]
    args.auto_file = args.auto_file.replace("{operator}", args.function)
    args.dump_file = args.dump_file.replace("{operator}", args.function)
    args.dump_py_file = args.dump_py_file.replace("{operator}", args.function)
    args.module_file = args.module_file.replace("{operator}", args.function)

    vsize = vsizes[args.arch]
    thrput = args.ipc * vsize//np.dtype(args.dtype).itemsize//8 * args.threads
    peak_time = tuned_mintime(*tuned_args, gflops=thrput*args.freq)
    print(f"Peak time (ms): {peak_time * 1000}")

    args.run_timeout = args.run_timeout if args.run_timeout is not None else int(1 + peak_time * 20)
    args.run_timeout += 10 if args.remote else 2 # Comm and setup minimal time

    if args.auto:
        # Auto scheduler, defines the task
        task = auto_scheduler.SearchTask(func=tuned_func, args=tuned_args, target=target)
        if args.auto_search:
            # search
            search_best(task, args)

        ## Get best found in search log and apply to get schedule
        inp, res = auto_scheduler.measure_record.load_best_record(args.auto_file, task.workload_key, target)
        assert inp is not None, f"not record found in {args.auto_file}"
        logger.debug("Best record: %s", inp.serialize()[0])
        sch, params = task.compute_dag.apply_steps_from_state(inp.state, task.layout_rewrite_option)
        # Output python schedule
        with open(args.dump_py_file, "w") as dumpf:
            dumpf.write(task.compute_dag.print_python_code_from_state(inp.state))
        tuned_results = [float(cost) for cost in res.costs]
        tuned_time = min(tuned_results)
        print(f"Tuned Execution time (ms): {tuned_time * 1000}")
        print(f"Tuned Peak performance: {peak_time / tuned_time * 100:.2f}%")
    else:
        # Default definition and schedule
        params = tuned_func(*tuned_args)

        # Create basic schedule for the output tensor operation
        sch = te.create_schedule(params[0].op)


    # Output schedule
    with open(args.dump_file, "w") as dumpf:
        dumpf.write(str(tvm.lower(sch, params, simple_mode=True)))

    if args.remote is not None:
        return

    # Build for the target
    # Give schedule, input/output tensors, target and name
    test_matmult = tvm.build(sch, params, target, name="test_matmult")

    # That's it now the test_matmult() function is a wrapper to the
    # generated implementation

    # Output library
    test_matmult.export_library(args.module_file)

    # Run reference implementation
    ref_out, ref_ins = ref_func(*tuned_args)

    # Test generaing inputs/outputs for the device and executing function
    a = tvm.nd.array(ref_ins[0], dev)
    b = tvm.nd.array(ref_ins[1], dev)
    c = tvm.nd.array(np.zeros((N, M), dtype=ref_out.dtype), dev)

    # wrap with an evaluation function for timings
    eval_func = test_matmult.time_evaluator(test_matmult.entry_name, dev,
                                            repeat=10,
                                            number=1,
                                            min_repeat_ms=100)

    # Here we run (note that compilation may happen here as it is lazy)
    results = eval_func(c, a, b)
    elapsed_time = min(results.results)
    print(f"Execution time (ms): {elapsed_time * 1000}")
    print(f"Peak performance: {peak_time / elapsed_time * 100:.2f}%")

    # Assert result is what we expect: TODO: implement in numpy
    assert np.allclose(c.numpy(), ref_out), f"the computed result mismatchs"
    print("All went well")

if __name__ == "__main__":
    main()
