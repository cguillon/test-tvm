#!/usr/bin/env python3
"""
Simple scheduling example for a convolution+bias+relu TVM Tensor Expressions.

See also TVM tutorial for more details:
https://tvm.apache.org/docs/tutorial/tensor_expr_get_started.html

Dependencies:
    - first build TVM in ~/tvm for instance, ref to https://gitlab.inria.fr/CORSE/parioopt#step-2-install-tvm
    - then install requirements
      pip install -r ~/tvm/python/requirements/core.txt
      pip install -r requirements.txt
    - then export python path
      export PYTHONPATH=$HOME/tvm/python

"""

import tvm
from tvm import te
from tvm import auto_scheduler
import numpy as np
from pathlib import Path
import logging
import argparse

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description="convolution schedule")
parser.add_argument("--auto", action="store_true", help="use auto-scheduler schedule")
parser.add_argument("--auto-search", action="store_true", help="search solutions with auto-scheduler")
parser.add_argument("--auto-reset", action="store_true", help="reset auto-scheduler search")
parser.add_argument("--auto-file", default="search-conv.log", help="auto scheduler search log file")
parser.add_argument("--batch", type=int, default=64, help="batch size")
parser.add_argument("--epoch", type=int, default=5, help="number of epochs")
parser.add_argument("--debug", action="store_true", help="debug mode")
args = parser.parse_args()

logging.basicConfig()
if args.debug:
    logger.setLevel(logging.DEBUG)

# Define target (needed for build), here the host CPU archi built with llvm
tgt = tvm.target.Target(target="llvm", host="llvm")

# Define a device to run on (needed for run), here the host CPU
dev = tvm.device(tgt.kind.name, 0)

# Convolution parameters (X and Y are size of output)
X, Y, CIN, COUT = 112, 112, 32, 32
SX = SY = 1
KX = KY = 3
AX, AY = (X-1)*SX + KX-1 + 1, (Y-1)*SY + KY-1 + 1


@auto_scheduler.register_workload
def convolution(X, Y, CIN, COUT, KX, KY, SX, SY, dtype):
    A = te.placeholder((AX, AY, CIN), name="A", dtype=dtype)
    W = te.placeholder((KX, KY, CIN, COUT), name="W", dtype=dtype)
    BIAS = te.placeholder((X,Y,COUT), name="BIAS", dtype=dtype)

    # Define output tensor, as an expression of inputs
    # Give shape, operation (as a lambda on an element) and name
    # Note that the reduction axis is specified for the te.sum() accumulator
    cin = te.reduce_axis((0, CIN), "cin")
    kx = te.reduce_axis((0, KX), "kx")
    ky = te.reduce_axis((0, KY), "ky")
    CONV = te.compute((X, Y, COUT), lambda x, y, cout: te.sum(A[x * SX + kx, y * SY + ky, cin] * W[kx, ky, cin, cout], axis=[kx, ky, cin]), name="CONV")
    CB = te.compute((X, Y, COUT), lambda x, y, cout: CONV(x, y, cout) + BIAS(x, y, cout), name="CB")
    C = te.compute((X, Y, COUT), lambda x, y, cout: tvm.tir.Select(CB(x, y, cout) < 0, 0.0, CB(x, y, cout)), name="C")
    return [A, W, BIAS, C]

def search_best(task, args):
    class LogCallback(auto_scheduler.measure.PythonBasedMeasureCallback):
        def callback(self, policy, inputs, results):
            for measure in results:
                time = np.mean([r.value for r in measure.costs])
                logger.debug(f"Execution time of candidate (ms): {time*1000:.3f}")

    if args.auto_reset:
        logger.debug(f"Clear schedules file {args.auto_file}")
        Path(args.auto_file).unlink(missing_ok=True)
    logger.debug(f"Search best schedule, store to {args.auto_file}")
    verbose = 100 if args.debug else 0
    batch, epoch, auto_file = args.batch, args.epoch, args.auto_file
    tune_option = auto_scheduler.TuningOptions(
        num_measure_trials=batch * epoch,
        num_measures_per_round=batch,
        runner=auto_scheduler.LocalRunner(
            repeat=5, number=1, min_repeat_ms=100
        ),
        measure_callbacks=[auto_scheduler.RecordToFile(auto_file), LogCallback()],
        verbose=verbose,
    )
    cost_model = auto_scheduler.XGBModel(num_warmup_sample=batch)  # default: 100
    search_policy = auto_scheduler.SketchPolicy(
        task,
        cost_model,
        seed=0,
        verbose=verbose,
        params=dict(
            sample_init_min_population=50,  # default: 50
            evolutionary_search_population=2048,  # default: 2048
            evolutionary_search_num_iters=4,  # default: 4
            sample_init_use_measured_ratio=0.2,  # default: 0.2
        ),
    )
    task.tune(tune_option, search_policy=search_policy)

conv_args = (X, Y, CIN, COUT, KX, KY, SX, SY, "float32")

if args.auto:
    # Auto scheduler, defines the task
    task = auto_scheduler.SearchTask(func=convolution, args=conv_args, target=tgt)
    if args.auto_search:
        # search
        search_best(task, args)

    # Get best found in search log and apply to get schedule
    inp, res = auto_scheduler.measure_record.load_best_record(args.auto_file, task.workload_key, tgt)
    s, out = task.compute_dag.apply_steps_from_state(inp.state)
    A, W, BIAS, C = out
else:
    # Default definition and schedule
    out = convolution(*conv_args)
    A, W, BIAS, C = out

    # Create basic schedule for the output tensor operation
    s = te.create_schedule(C.op)


# Output schedule
print(tvm.lower(s, out, simple_mode=True))


# Build for the target
# Give schedule, input/output tensors, target and name
test_conv = tvm.build(s, out, tgt, name="test_conv")

# That's it now the test_conv() function is a wrapper to the
# generated implementation

# Test generaing inputs/outputs for the device and executing function
a = tvm.nd.array(np.random.uniform(size=AX*AY*CIN).astype(A.dtype).reshape((AX, AY, CIN)), dev)
w = tvm.nd.array(np.random.uniform(size=KX*KY*CIN*COUT).astype(W.dtype).reshape((KX, KY, CIN, COUT)), dev)
b = tvm.nd.array(np.random.uniform(size=X*Y*COUT).astype(BIAS.dtype).reshape((X, Y, COUT)), dev)
c = tvm.nd.array(np.zeros(X*Y*COUT, dtype=C.dtype).reshape((X,Y,COUT)), dev)

# wrap with an evaluation function for timings
eval_func = test_conv.time_evaluator(test_conv.entry_name, dev,
                                    repeat=10,
                                    number=1,
                                    min_repeat_ms=100)

# Here we run (note that compilation may happen here as it is lazy)
results = eval_func(a, w, b, c)
print(f"Execution time (ms): {results.mean * 1000}")

# Assert result is what we expect: TODO: implement in numpy
#assert np.allclose(c.numpy(), a.numpy() @ b.numpy()), f"the computed result mismatchs"
#print("All went well")
