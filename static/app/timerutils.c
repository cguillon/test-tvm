#include <tvm/runtime/crt/platform.h>
#include <timerutils.h>

void TimerStart(void) {
    TVMPlatformTimerStart();
}

void TimerStop(double *elapsed) {
    TVMPlatformTimerStop(elapsed);
}

