#include <string.h>
#include <stdlib.h>
#include <ndutils.h>

#include <tvm/runtime/crt/internal/common/ndarray.h>
#include <tvm/runtime/crt/page_allocator.h>
#include <tvm/runtime/crt/platform.h>

static DLDataType NDType2DLType(int ndtype) {
    DLDataType dltype = {0, 0};
    switch (ndtype) {
    case NDTYPE_float32:
        dltype.code = kDLFloat;
        dltype.bits = 32;
        break;
    case NDTYPE_float64:
        dltype.code = kDLFloat;
        dltype.bits = 64;
        break;
    default:
        break;
    }
    return dltype;
}

void *ND_Createv(int dtype, int ndim, va_list va) {
    TVMNDArray *array;
    int64_t *shape;
    DLDevice dldev = {kDLCPU, 0};
    DLDataType dltype = NDType2DLType(dtype);
    tvm_crt_error_t err;
    err = TVMPlatformMemoryAllocate(sizeof(*array), dldev, (void **)&array);
    if (err != kTvmErrorNoError) {
        return NULL;
    }
    err = TVMPlatformMemoryAllocate(sizeof(int64_t) * ndim, dldev, (void **)&shape);
    if (err != kTvmErrorNoError) {
        return NULL;
    }
    memset(array, 0, sizeof(*array));
    array->dl_tensor.ndim = ndim;
    array->dl_tensor.shape = shape;
    for (int d = 0; d < ndim; d++) {
        array->dl_tensor.shape[d] = va_arg(va, int);
    }
    array->dl_tensor.dtype = dltype;
    array->dl_tensor.device = dldev;
    array->dl_tensor.data = 0;
    return array;
}

void *ND_Wrapv(void *data, int dtype, int ndim, va_list va) {
    TVMNDArray *array;
    array = ND_Createv(dtype, ndim, va);
    if (array == NULL) {
        return NULL;
    }
    array->dl_tensor.data = data;
    return array;
}

void *ND_Emptyv(int dtype, int ndim, va_list va) {
    TVMNDArray *array = ND_Createv(dtype, ndim, va);
    if (array == NULL) {
        return NULL;
    }
    int64_t total_elem_bytes = TVMNDArray_DataSizeBytes(array);
    array->dl_tensor.data =
        TVMBackendAllocWorkspace(kDLCPU, 0, total_elem_bytes,
                                 array->dl_tensor.dtype.code,
                                 array->dl_tensor.dtype.bits);
    return array;
}

void *ND_Zerov(int dtype, int ndim, va_list va) {
    TVMNDArray *array = ND_Createv(dtype, ndim, va);
    if (array == NULL) {
        return NULL;
    }
    int64_t total_elem_bytes = TVMNDArray_DataSizeBytes(array);
    array->dl_tensor.data =
        TVMBackendAllocWorkspace(kDLCPU, 0, total_elem_bytes,
                                 array->dl_tensor.dtype.code,
                                 array->dl_tensor.dtype.bits);
    memset(array->dl_tensor.data, 0, total_elem_bytes);
    return array;
}

void *ND_Create(int dtype, int ndim, ...) {
    va_list va;
    va_start(va, ndim);
    void *array = ND_Createv(dtype, ndim, va);
    va_end(va);
    return array;
}

void *ND_Wrap(void *data, int dtype, int ndim, ...) {
    va_list va;
    va_start(va, ndim);
    void *array = ND_Wrapv(data, dtype, ndim, va);
    va_end(va);
    return array;
}

void *ND_Empty(int dtype, int ndim, ...) {
    va_list va;
    va_start(va, ndim);
    void *array = ND_Emptyv(dtype, ndim, va);
    va_end(va);
    return array;
}

void *ND_Zero(int dtype, int ndim, ...) {
    va_list va;
    va_start(va, ndim);
    void *array = ND_Zerov(dtype, ndim, va);
    va_end(va);
    return array;
}

void ND_Release(void *array) {
    TVMNDArray *arr = array;
    DLDevice dev = {kDLCPU, 0};

    if (TVMNDArray_DecrementReference(arr) > 0) {
        return;
    }
    TVMPlatformMemoryFree(arr->dl_tensor.data, dev);
    TVMPlatformMemoryFree(arr->dl_tensor.shape, dev);
    TVMPlatformMemoryFree(arr, dev);
}

void ND_Unwrap(void *array) {
    TVMNDArray *arr = array;
    DLDevice dev = {kDLCPU, 0};

    if (TVMNDArray_DecrementReference(arr) > 0) {
        return;
    }

    TVMPlatformMemoryFree(arr->dl_tensor.shape, dev);
    TVMPlatformMemoryFree(arr, dev);
}
