#ifndef _MODEL_FORWARD_H
#define _MODEL_FORWARD_H

extern void model_forward(void);
extern void model_fini(void);
extern void model_init(void);

#endif
