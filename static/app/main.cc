#include <iostream>
#include <cassert>

#include <platform_arch.h>
#include <ndutils.h>
#include <timerutils.h>
#include <compute.h>
#include <model_forward.h>

int main()
{
    double elapsed, peak;

    model_init();

    TimerStart();
    model_forward();
    TimerStop(&elapsed);

    model_fini();

    peak = COMPUTE_NUM/ARCH_CPS/(ARCH_VECBITS/COMPUTE_ELTBITS)/ARCH_IPC;
    std::cout << "peak_ms: " << peak*1000 << std::endl;
    std::cout << "elpased_ms: " << elapsed*1000 << std::endl;
    std::cout << "peak_pct: " << peak/elapsed*100 << std::endl;

    return 0;
}
