#include <tvm/runtime/crt/platform.h>

static int TVMPlatformInitialized;

void  __attribute__((constructor)) Platform_Init(void) {
    if (!TVMPlatformInitialized) {
        TVMPlatformInitialized++;
        TVMPlatformInitialize();
    }
}

void __attribute__((destructor)) Platform_Fini(void) {
}
