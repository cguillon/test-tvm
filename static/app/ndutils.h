#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>

    extern void *ND_Create(int dtype, int ndim, ...);
    extern void *ND_Wrap(void *data, int dtype, int ndim, ...);
    extern void *ND_Empty(int dtype, int ndim, ...);
    extern void *ND_Zero(int dtype, int ndim, ...);
    extern void ND_Release(void *a);
    extern void ND_ReleaseHandler(void *a);

    extern void *ND_Createv(int dtype, int ndim, va_list va);
    extern void *ND_Wrapv(void *data, int dtype, int ndim, va_list va);
    extern void *ND_Emptyv(int dtype, int ndim, va_list va);
    extern void *ND_Zerov(int dtype, int ndim, va_list va);

#define NDTYPE_float32 1
#define NDTYPE_float64 2

#define ND_TVMType 7U // TVM type id: kTVMDLTensorHandle

#ifdef __cplusplus
} // extern "C"
#endif
