#ifdef __cplusplus
extern "C" {
#endif

    extern void TimerStart(void);
    extern void TimerStop(double *elapsed);

#define ND_Type 7U // TVM type id: kTVMDLTensorHandle

#ifdef __cplusplus
} // extern "C"
#endif
