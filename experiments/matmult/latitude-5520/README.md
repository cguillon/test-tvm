Experiments
===========

Run on cguillon latitude-5520 (AVX512 n_fma = 1, 4Ghz):
- single thread
- hyperthreading enabled
- freq scaling untouched (freq 4.0GHZ)
- TVM version 0.16 compiled with clang backend clang-config-14
- Set TVM with: `export PYTHONPATH=/home/cguillon/tvm/python`


Results obtained with matmul (N, M, K, dtype) == (256, 2048, 1001, "float32") on AVX512:

    # matmul with B layout [k, j]
    /usr/bin/time -p ./schedule-matmult-auto.py --function matmul --freq 4.0 --ipc 1 --arch avx512 --dims 256 2048 1001 --auto --auto-reset --auto-search --epoch 10 --debug
    Tuned Execution time (ms): 8.811
    Tuned Peak performance: 93.07%
    Execution time (ms): 9.7096835
    Peak performance: 84.45%
    All went well
    real 454.88
    user 2052.62
    sys 93.04

Note that tuned peak perf is surprisingly high, possibly due to the fact that the machine as a single FMA/cycle, hence easier to reach.

Ref to results files:

- `search-*.log`: TVM schedule log file
- `search-*-dump.txt`: TVM best schedule pretyy print
