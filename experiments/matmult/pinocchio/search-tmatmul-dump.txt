# from tvm.script import ir as I
# from tvm.script import tir as T

@I.ir_module
class Module:
    @T.prim_func
    def main(A: T.Buffer((512, 128), "float32"), B: T.Buffer((256, 128), "float32"), out: T.Buffer((512, 256), "float32")):
        T.func_attr({"from_legacy_te_schedule": T.bool(True), "tir.noalias": T.bool(True)})
        auto_scheduler_layout_transform = T.allocate([32768], "float32", "global")
        auto_scheduler_layout_transform_1 = T.Buffer((32768,), data=auto_scheduler_layout_transform)
        for ax0_ax1_fused_ax2_fused in T.parallel(64):
            for ax4, ax5 in T.grid(8, 64):
                B_1 = T.Buffer((32768,), data=B.data)
                auto_scheduler_layout_transform_1[ax0_ax1_fused_ax2_fused * 512 + ax4 * 64 + ax5] = B_1[ax0_ax1_fused_ax2_fused // 16 * 8192 + ax5 * 128 + ax0_ax1_fused_ax2_fused % 16 * 8 + ax4]
        for i_outer_outer_outer_j_outer_outer_outer_fused_i_outer_outer_inner_fused in T.parallel(512):
            cse_var_1: T.int32 = i_outer_outer_outer_j_outer_outer_outer_fused_i_outer_outer_inner_fused % 128 * 1024 + i_outer_outer_outer_j_outer_outer_outer_fused_i_outer_outer_inner_fused // 128 * 64
            out_1 = T.Buffer((131072,), data=out.data)
            out_1[cse_var_1:cse_var_1 + 64] = T.Broadcast(T.float32(0), 64)
            out_1[cse_var_1 + 256:cse_var_1 + 256 + 64] = T.Broadcast(T.float32(0), 64)
            out_1[cse_var_1 + 512:cse_var_1 + 512 + 64] = T.Broadcast(T.float32(0), 64)
            out_1[cse_var_1 + 768:cse_var_1 + 768 + 64] = T.Broadcast(T.float32(0), 64)
            for k_outer in range(16):
                cse_var_13: T.int32 = cse_var_1 + 768
                cse_var_12: T.int32 = cse_var_1 + 512
                cse_var_11: T.int32 = cse_var_1 + 256
                cse_var_10: T.int32 = i_outer_outer_outer_j_outer_outer_outer_fused_i_outer_outer_inner_fused % 128 * 512 + k_outer * 8
                cse_var_9: T.int32 = i_outer_outer_outer_j_outer_outer_outer_fused_i_outer_outer_inner_fused // 128 * 8192 + k_outer * 512
                cse_var_8: T.int32 = cse_var_9 + 64
                cse_var_7: T.int32 = cse_var_9 + 448
                cse_var_6: T.int32 = cse_var_9 + 384
                cse_var_5: T.int32 = cse_var_9 + 320
                cse_var_4: T.int32 = cse_var_9 + 256
                cse_var_3: T.int32 = cse_var_9 + 192
                cse_var_2: T.int32 = cse_var_9 + 128
                A_1 = T.Buffer((65536,), data=A.data)
                out_1[cse_var_1:cse_var_1 + 64] = out_1[cse_var_1:cse_var_1 + 64] + T.Broadcast(A_1[cse_var_10], 64) * auto_scheduler_layout_transform_1[cse_var_9:cse_var_9 + 64]
                out_1[cse_var_11:cse_var_11 + 64] = out_1[cse_var_11:cse_var_11 + 64] + T.Broadcast(A_1[cse_var_10 + 128], 64) * auto_scheduler_layout_transform_1[cse_var_9:cse_var_9 + 64]
                out_1[cse_var_12:cse_var_12 + 64] = out_1[cse_var_12:cse_var_12 + 64] + T.Broadcast(A_1[cse_var_10 + 256], 64) * auto_scheduler_layout_transform_1[cse_var_9:cse_var_9 + 64]
                out_1[cse_var_13:cse_var_13 + 64] = out_1[cse_var_13:cse_var_13 + 64] + T.Broadcast(A_1[cse_var_10 + 384], 64) * auto_scheduler_layout_transform_1[cse_var_9:cse_var_9 + 64]
                out_1[cse_var_1:cse_var_1 + 64] = out_1[cse_var_1:cse_var_1 + 64] + T.Broadcast(A_1[cse_var_10 + 1], 64) * auto_scheduler_layout_transform_1[cse_var_8:cse_var_8 + 64]
                out_1[cse_var_11:cse_var_11 + 64] = out_1[cse_var_11:cse_var_11 + 64] + T.Broadcast(A_1[cse_var_10 + 129], 64) * auto_scheduler_layout_transform_1[cse_var_8:cse_var_8 + 64]
                out_1[cse_var_12:cse_var_12 + 64] = out_1[cse_var_12:cse_var_12 + 64] + T.Broadcast(A_1[cse_var_10 + 257], 64) * auto_scheduler_layout_transform_1[cse_var_8:cse_var_8 + 64]
                out_1[cse_var_13:cse_var_13 + 64] = out_1[cse_var_13:cse_var_13 + 64] + T.Broadcast(A_1[cse_var_10 + 385], 64) * auto_scheduler_layout_transform_1[cse_var_8:cse_var_8 + 64]
                out_1[cse_var_1:cse_var_1 + 64] = out_1[cse_var_1:cse_var_1 + 64] + T.Broadcast(A_1[cse_var_10 + 2], 64) * auto_scheduler_layout_transform_1[cse_var_2:cse_var_2 + 64]
                out_1[cse_var_11:cse_var_11 + 64] = out_1[cse_var_11:cse_var_11 + 64] + T.Broadcast(A_1[cse_var_10 + 130], 64) * auto_scheduler_layout_transform_1[cse_var_2:cse_var_2 + 64]
                out_1[cse_var_12:cse_var_12 + 64] = out_1[cse_var_12:cse_var_12 + 64] + T.Broadcast(A_1[cse_var_10 + 258], 64) * auto_scheduler_layout_transform_1[cse_var_2:cse_var_2 + 64]
                out_1[cse_var_13:cse_var_13 + 64] = out_1[cse_var_13:cse_var_13 + 64] + T.Broadcast(A_1[cse_var_10 + 386], 64) * auto_scheduler_layout_transform_1[cse_var_2:cse_var_2 + 64]
                out_1[cse_var_1:cse_var_1 + 64] = out_1[cse_var_1:cse_var_1 + 64] + T.Broadcast(A_1[cse_var_10 + 3], 64) * auto_scheduler_layout_transform_1[cse_var_3:cse_var_3 + 64]
                out_1[cse_var_11:cse_var_11 + 64] = out_1[cse_var_11:cse_var_11 + 64] + T.Broadcast(A_1[cse_var_10 + 131], 64) * auto_scheduler_layout_transform_1[cse_var_3:cse_var_3 + 64]
                out_1[cse_var_12:cse_var_12 + 64] = out_1[cse_var_12:cse_var_12 + 64] + T.Broadcast(A_1[cse_var_10 + 259], 64) * auto_scheduler_layout_transform_1[cse_var_3:cse_var_3 + 64]
                out_1[cse_var_13:cse_var_13 + 64] = out_1[cse_var_13:cse_var_13 + 64] + T.Broadcast(A_1[cse_var_10 + 387], 64) * auto_scheduler_layout_transform_1[cse_var_3:cse_var_3 + 64]
                out_1[cse_var_1:cse_var_1 + 64] = out_1[cse_var_1:cse_var_1 + 64] + T.Broadcast(A_1[cse_var_10 + 4], 64) * auto_scheduler_layout_transform_1[cse_var_4:cse_var_4 + 64]
                out_1[cse_var_11:cse_var_11 + 64] = out_1[cse_var_11:cse_var_11 + 64] + T.Broadcast(A_1[cse_var_10 + 132], 64) * auto_scheduler_layout_transform_1[cse_var_4:cse_var_4 + 64]
                out_1[cse_var_12:cse_var_12 + 64] = out_1[cse_var_12:cse_var_12 + 64] + T.Broadcast(A_1[cse_var_10 + 260], 64) * auto_scheduler_layout_transform_1[cse_var_4:cse_var_4 + 64]
                out_1[cse_var_13:cse_var_13 + 64] = out_1[cse_var_13:cse_var_13 + 64] + T.Broadcast(A_1[cse_var_10 + 388], 64) * auto_scheduler_layout_transform_1[cse_var_4:cse_var_4 + 64]
                out_1[cse_var_1:cse_var_1 + 64] = out_1[cse_var_1:cse_var_1 + 64] + T.Broadcast(A_1[cse_var_10 + 5], 64) * auto_scheduler_layout_transform_1[cse_var_5:cse_var_5 + 64]
                out_1[cse_var_11:cse_var_11 + 64] = out_1[cse_var_11:cse_var_11 + 64] + T.Broadcast(A_1[cse_var_10 + 133], 64) * auto_scheduler_layout_transform_1[cse_var_5:cse_var_5 + 64]
                out_1[cse_var_12:cse_var_12 + 64] = out_1[cse_var_12:cse_var_12 + 64] + T.Broadcast(A_1[cse_var_10 + 261], 64) * auto_scheduler_layout_transform_1[cse_var_5:cse_var_5 + 64]
                out_1[cse_var_13:cse_var_13 + 64] = out_1[cse_var_13:cse_var_13 + 64] + T.Broadcast(A_1[cse_var_10 + 389], 64) * auto_scheduler_layout_transform_1[cse_var_5:cse_var_5 + 64]
                out_1[cse_var_1:cse_var_1 + 64] = out_1[cse_var_1:cse_var_1 + 64] + T.Broadcast(A_1[cse_var_10 + 6], 64) * auto_scheduler_layout_transform_1[cse_var_6:cse_var_6 + 64]
                out_1[cse_var_11:cse_var_11 + 64] = out_1[cse_var_11:cse_var_11 + 64] + T.Broadcast(A_1[cse_var_10 + 134], 64) * auto_scheduler_layout_transform_1[cse_var_6:cse_var_6 + 64]
                out_1[cse_var_12:cse_var_12 + 64] = out_1[cse_var_12:cse_var_12 + 64] + T.Broadcast(A_1[cse_var_10 + 262], 64) * auto_scheduler_layout_transform_1[cse_var_6:cse_var_6 + 64]
                out_1[cse_var_13:cse_var_13 + 64] = out_1[cse_var_13:cse_var_13 + 64] + T.Broadcast(A_1[cse_var_10 + 390], 64) * auto_scheduler_layout_transform_1[cse_var_6:cse_var_6 + 64]
                out_1[cse_var_1:cse_var_1 + 64] = out_1[cse_var_1:cse_var_1 + 64] + T.Broadcast(A_1[cse_var_10 + 7], 64) * auto_scheduler_layout_transform_1[cse_var_7:cse_var_7 + 64]
                out_1[cse_var_11:cse_var_11 + 64] = out_1[cse_var_11:cse_var_11 + 64] + T.Broadcast(A_1[cse_var_10 + 135], 64) * auto_scheduler_layout_transform_1[cse_var_7:cse_var_7 + 64]
                out_1[cse_var_12:cse_var_12 + 64] = out_1[cse_var_12:cse_var_12 + 64] + T.Broadcast(A_1[cse_var_10 + 263], 64) * auto_scheduler_layout_transform_1[cse_var_7:cse_var_7 + 64]
                out_1[cse_var_13:cse_var_13 + 64] = out_1[cse_var_13:cse_var_13 + 64] + T.Broadcast(A_1[cse_var_10 + 391], 64) * auto_scheduler_layout_transform_1[cse_var_7:cse_var_7 + 64]