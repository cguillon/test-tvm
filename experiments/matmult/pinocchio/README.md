Experiments
===========

Run on pinocchio with:
- single thread
- hyperthreading disabled
- freq scaling disabled (freq 2.1GHZ)
- TVM version 0.16 compiled with clang backend clang-config-12
- Set TVM with: `export PYTHONPATH=/opt/local/tvm/tvm-v0.16.0.rc0/python`


Results obtained with matmul (N, L, M, dtype) == (512, 128, 256, "float32") on AVX512:

    # matmul with B layout [k, j]
    env TVM_NUM_THREADS=1 ./schedule-matmult-auto.py --debug --auto --batch 64 --epoch 16 --auto-reset --auto-search --function matmul
    Execution time (ms): 0.2990042728971963
    Peak performance: 83.50%
    
    # matmul with B layout [j, k] and layout free option
    env TVM_NUM_THREADS=1 ./schedule-matmult-auto.py --debug --auto --batch 64 --epoch 16 --auto-reset --auto-search --function tmatmul
    Execution time (ms): 0.28419422779783393
    Peak performance: 87.85%
    
    # matmul with B layout [j, k] without layout free option
    env TVM_NUM_THREADS=1 ./schedule-matmult-auto.py --debug --auto --batch 64 --epoch 16 --auto-reset --auto-search --function tmatmulnf
    Execution time (ms): 0.36387138863636365
    Peak performance: 68.61%

Ref to results files:

- `search-*.log`: TVM schedule log file
- `search-*-dump.txt`: TVM best schedule pretyy print
