Experiments
===========

Run on corse-rpi0 with:
- single thread
- freq scaling disabled (freq 1.5GHZ)
- TVM version 0.16 compiled with clang backend clang-config-11
- Set TVM with: `export PYTHONPATH=/home/cguillon/tvm/python`


Results obtained with matmul (N, M, K, dtype) == (256, 2048, 1001, "float32") on aarch64 NEON cortex-a72:

    # matmul with B layout [K, M]: search-matmul.log (640 executions)
    ./schedule-matmult-auto.py --function matmul --freq 1.5 --ipc 1 --arch aarch64 --dims 256 2048 1001 --auto --auto-reset --auto-search --epoch 10 --debug
    Peak time (ms): 87.46871466666667
    ...
    Execution time (ms): 157.8777565
    Peak performance: 55.40%

Replay with:

    ./schedule-matmult-auto.py --function matmul --freq 1.5 --ipc 1 --arch aarch64 --dims 256 2048 1001 --auto --debug
    DEBUG:__main__:Executing child command: env TVM_NUM_THREADS=1 setarch -R -- ./schedule-matmult-auto.py --child --freq 1.5 --ipc 1 --arch aarch64 --dims 256 2048 1001 --auto --debug
    Peak time (ms): 87.46871466666667
    DEBUG:__main__:Best record: [["[\"matmul\", 256, 2048, 1001, \"float32\"]", "llvm -keys=arm_cpu,cpu -mattr=+neon -mcpu=cortex-a72 -mtriple=aarch64-linux-gnu", [1, 64, 64, 0, 0, 0, 0, 0], "", 1, []], [[], [["CHW", 2, "local"], ["SP", 2, 0, 256, [2, 32, 4], 1], ["SP", 2, 4, 2048, [64, 2, 8], 1], ["SP", 2, 8, 1001, [13], 1], ["RE", 2, [0, 4, 1, 5, 8, 2, 6, 9, 3, 7]], ["FSP", 3, 0, 1, 2], ["FSP", 3, 3, 2, 2], ["RE", 3, [0, 3, 1, 4, 2, 5]], ["CA", 2, 3, 3], ["FU", 3, [0, 1, 2, 3]], ["AN", 3, 0, 3], ["PR", 2, 0, "auto_unroll_max_step$512"], ["AN", 2, 9, 2], ["AN", 3, 2, 2]]]]
    Execution time (ms): 157.8777565
    Peak performance: 55.40%
    All went well

Ref to results files:

- `search-*.log`: TVM schedule log file
- `search-*-dump.txt`: TVM best schedule pretyy print

Remote execution:
- launch on corse-rpi0: scripts/launch_tracker.sh and scripts/launch_server.sh
- launch on the host: scripts/launch_ssh_relay.sh

Execute from the host:

    /usr/bin/time -p ./schedule-matmult-auto.py --function matmul --freq 1.5 --ipc 1 --arch aarch64 --dims 256 2048 1001 --auto --auto-reset --auto-search --epoch 10 --debug --remote 127.0.0.1 --run-timeout 10
    DEBUG:__main__:Best record: [["[\"matmul\", 256, 2048, 1001, \"float32\"]", "llvm -keys=arm_cpu,cpu -mattr=+neon -mcpu=cortex-a72 -mtriple=aarch64-linux-gnu", [1, 64, 64, 0, 0, 0, 0, 0], "", 1, []], [[], [["CHW", 2, "local"], ["SP", 2, 0, 256, [1, 32, 2], 1], ["SP", 2, 4, 2048, [4, 4, 16], 1], ["SP", 2, 8, 1001, [13], 1], ["RE", 2, [0, 4, 1, 5, 8, 2, 6, 9, 3, 7]], ["FSP", 3, 0, 1, 2], ["FSP", 3, 3, 2, 2], ["RE", 3, [0, 3, 1, 4, 2, 5]], ["CA", 2, 3, 3], ["FU", 3, [0, 1]], ["AN", 3, 0, 3], ["PR", 2, 0, "auto_unroll_max_step$512"], ["AN", 2, 9, 2], ["AN", 3, 4, 2]]]]
    Tuned Execution time (ms): 163.311
    Tuned Peak performance: 53.56%
    real 2283.10
    user 1609.46
    sys 64.69

Ref to remote execution results files:
- `search-*-remote.log`: TVM schedule log file
- `search-*-dump-remote.txt`: TVM best schedule pretyy print

